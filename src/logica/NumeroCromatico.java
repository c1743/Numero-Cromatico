package logica;

import java.util.ArrayList;

import nodo.GrafoConColor;

/*
 * 
 * @author AndresFWilT
 * 
 */

public class NumeroCromatico {

	private ArrayList<int[]> listas;
	private int[][] adyacencias;
	private int[] orden;
	private int i, j;
	private ArrayList<Integer> permutaciones;

	public NumeroCromatico() {

		listas = new ArrayList<>();
	}

	public void setAdyacencias(int[][] adyacencias) {

		this.adyacencias = adyacencias;
		iniciarOrden();
	}

	private void iniciarOrden() {
		orden = new int[adyacencias.length];

		for (i = 0; i < orden.length; i++) {
			orden[i] = i;
		}
	}

	public void verificarAdyaadyacenciascencias() {

		int[] listaUno = new int[adyacencias.length];
		inicializarLista(listaUno);
		listaUno[0] = 0;

		obtenerNumeroDeListas();

		// Se verifica si es bipartido
		if (listas.size() != 2) {

		}
	}

	private int obtenerNumeroDeListas() {
		int listaDisponible;

		i = 1;
		while (orden.length != i) {
			for (j = 0; j < adyacencias[orden[i]].length; j++) {

				if (adyacencias[orden[i]][j] == 1) {
					listaDisponible = verificarListas(orden[i]);
					if (listaDisponible == -1) {
						int[] nuevaLista = new int[adyacencias.length];
						inicializarLista(nuevaLista);
						nuevaLista[0] = orden[i];
						listas.add(nuevaLista);
					} else {
						listas.get(listaDisponible)[dondeIngresar(listaDisponible)] = orden[i];
					}
				}
			}
			i++;
		}
		return listas.size();
	}

	private void inicializarLista(int[] nuevaLista) {
		for (i = 0; i < nuevaLista.length; i++) {
			nuevaLista[i] = -1;
		}
	}

	private int dondeIngresar(int listaDisponible) {
		for (i = 0; i < listas.get(listaDisponible).length; i++) {
			if (listas.get(listaDisponible)[i] == -1)
				return i;
		}
		return -1;
	}

	private int verificarListas(int a) {
		int k = 0;
		boolean puedeEntrar = true;
		while (k != listas.size()) {

			for (i = 0; i < listas.get(k).length; i++) {
				if (listas.get(k)[i] == a) {
					puedeEntrar = false;
					break;
				}
			}
			if (puedeEntrar)
				return k;
			k++;
		}
		return -1;
	}

	// PERMUTACIONES

	public int getNumeroCromatico() {

		this.asimetrizar(adyacencias);
		GrafoConColor grafoColor = new GrafoConColor(adyacencias);
		ArrayList<Integer> candidatos = new ArrayList<Integer>();
		// Se obtiene un nodo desde el cual empezar el algoritmo
		int actual = orden[0];
		grafoColor.darColor(actual);
		permutaciones = new ArrayList<Integer>();
		permutaciones.add(actual);
		for (int i = 0; i < adyacencias.length; i++) {
			// Se obtienen los vecinos del actual
			int vecinos[] = grafoColor.getVecinos(actual);
			// Se agregan los vecinos a candidatos
			for (int k = 0; k < vecinos.length; k++) {
				if (!candidatos.contains(vecinos[k]) && grafoColor.getVertices().get(vecinos[k]).getColor() == -1) {
					candidatos.add(vecinos[k]);
				}
			}
			// Se selecciona un elemento de candidatos y se le da color
			if (candidatos.isEmpty() == false) {
				grafoColor.darColor(candidatos.get(0));
				actual = candidatos.get(0);
				candidatos.remove(0);
				permutaciones.add(actual);

			} else {
				break;
			}
		}
		return grafoColor.getColoresTotales();
	}

	private void asimetrizar(int[][] adyacencias) {
		for (int i = 0; i < adyacencias.length; i++) {
			for (int j = 0; j < adyacencias.length; j++) {
				if (i > j) {
					adyacencias[j][i] = adyacencias[i][j];
				}
			}
		}

	}

	public GrafoConColor getGrafoColor() {
		this.asimetrizar(adyacencias);
		GrafoConColor grafoColor = new GrafoConColor(adyacencias);
		ArrayList<Integer> candidatos = new ArrayList<Integer>();
		// Se obtiene un nodo desde el cual empezar el algoritmo
		int actual = orden[0];
		grafoColor.darColor(actual);
		permutaciones = new ArrayList<Integer>();
		permutaciones.add(actual);
		for (int i = 0; i < adyacencias.length; i++) {
			// Se obtienen los vecinos del actual
			int vecinos[] = grafoColor.getVecinos(actual);
			// Se agregan los vecinos a candidatos
			for (int k = 0; k < vecinos.length; k++) {
				if (!candidatos.contains(vecinos[k]) && grafoColor.getVertices().get(vecinos[k]).getColor() == -1) {
					candidatos.add(vecinos[k]);
				}
			}
			// Se selecciona un elemento de candidatos y se le da color
			if (candidatos.isEmpty() == false) {
				grafoColor.darColor(candidatos.get(0));
				actual = candidatos.get(0);
				candidatos.remove(0);
				permutaciones.add(actual);

			} else {
				break;
			}
		}
		return grafoColor;
	}

	public String getPermutaciones() {
		String salida = "";
		for (int i = 0; i < permutaciones.size(); i++) {
			if(i== permutaciones.size()-1) {
				salida += permutaciones.get(i) + 1;
			}else {
				salida += permutaciones.get(i) + 1;
				salida += ",";
			}
		}
		return salida;
	}

}