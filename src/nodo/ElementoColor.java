package nodo;

/*
 * 
 * @author AndresFWilT
 * 
 */

public class ElementoColor {
	int numero;
	int color;
	
	public ElementoColor(int numero, int color) {
		this.numero = numero;
		this.color = color;
	}

	public int getNumero() {
		return numero;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}
	
}
