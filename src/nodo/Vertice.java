package nodo;

/*
 * 
 * @author AndresFWilT
 * 
 */

public class Vertice {
    
    private int x, y, numero;

    public Vertice(int x, int y, int numero) {
        this.x = x;
        this.y = y;
        this.numero = numero;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

}
