package run;

import logica.NumeroCromatico;

public class Logica {

	public static void main(String[] args) {
		NumeroCromatico n = new NumeroCromatico();
		//El de por computador
		/*int [][] adya = new int[8][8];
		adya[0][1]=1;
		adya[1][0]=1;

		adya[0][2]=1;
		adya[2][0]=1;

		adya[0][4]=1;
		adya[4][0]=1;
		
		adya[1][5]=1;
		adya[5][1]=1;

		adya[1][3]=1;
		adya[3][1]=1;

		adya[2][6]=1;
		adya[6][2]=1;

		adya[2][3]=1;
		adya[3][2]=1;
		
		adya[3][7]=1;
		adya[7][3]=1;
		
		adya[4][5]=1;
		adya[5][4]=1;
		
		adya[4][6]=1;
		adya[6][4]=1;
		
		adya[5][7]=1;
		adya[7][5]=1;
		
		adya[6][7]=1;
		adya[7][6]=1;


	
		
		adya[0][0]=0;
		adya[1][1]=0;
		adya[2][2]=0;
		adya[3][3]=0;
		adya[4][4]=0;
		adya[5][5]=0;
		adya[6][6]=0;
		adya[7][7]=0;*/
		
		//Un triangulo
		int [][] adya = new int[3][3];
		
		adya[0][1]=1;
		adya[1][0]=11;
		
		adya[0][2]=1;
		adya[2][0]=11;

		adya[1][2]=1;
		adya[2][1]=1;
		
		//No es necesario colocar los ceros pues por defecto un int es 0
		
		n.setAdyacencias(adya);
		System.out.println(n.getNumeroCromatico());
	}

}
