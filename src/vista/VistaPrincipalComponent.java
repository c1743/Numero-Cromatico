package vista;

import java.util.ArrayList;

import logica.NumeroCromatico;
import nodo.ElementoColor;
import vista.componentes.barraSuperior.BarraSuperiorComponent;
import vista.componentes.creacionGrafo.PanelCreacionGrafoComponent;
import vista.componentes.panelResultado.PanelResultadoComponent;

/*
 * 
 * @author AndresFWilT
 * 
 */

public class VistaPrincipalComponent {
    
    private VistaPrincipalTemplate vistaPrincipalTemplate;
    private BarraSuperiorComponent barraSuperiorComponent;
    private PanelCreacionGrafoComponent panelCreacionGrafoComponent;
    private PanelResultadoComponent panelResultadoComponent;
    private NumeroCromatico numeroCromatico;

    public VistaPrincipalComponent() {

        numeroCromatico = new NumeroCromatico();
        vistaPrincipalTemplate = new VistaPrincipalTemplate();
        barraSuperiorComponent = new BarraSuperiorComponent(this);
        vistaPrincipalTemplate.gPBarraSuperior().add(barraSuperiorComponent.gBarraSuperiorTemplate());
        panelCreacionGrafoComponent = new PanelCreacionGrafoComponent(this);
        vistaPrincipalTemplate.gPCentral().add(panelCreacionGrafoComponent.gPanelCreacionGrafoTemplate());
        panelResultadoComponent = new PanelResultadoComponent(
            panelCreacionGrafoComponent.gPanelCreacionGrafoTemplate().getVertices()
        );
        vistaPrincipalTemplate.gPRecorridos().add(panelResultadoComponent.gPanelResultadoTemplate());
        vistaPrincipalTemplate.repaint();
    }

    public VistaPrincipalTemplate gVistaPrincipalTemplate() {
        return vistaPrincipalTemplate;
    }

	public void recibeAdyacencias(int[][] adyacencias) {

        numeroCromatico.setAdyacencias(adyacencias);
        ;
        panelResultadoComponent.dibujarResultados(
            adyacencias, numeroCromatico.getGrafoColor().getVertices(), numeroCromatico.getNumeroCromatico(), numeroCromatico.getPermutaciones()
        );
	}

	public void moverFrame(int i, int j) {
        vistaPrincipalTemplate.moverFrame(i, j);
	}
}
