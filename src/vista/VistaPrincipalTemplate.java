package vista;

import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * 
 * @author AndresFWilT
 * 
 */

public class VistaPrincipalTemplate extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel pBarraSuperior, pCentral, pRecorridos;
    
    public VistaPrincipalTemplate() {
    	this.setTitle("Numero Cromatico");
        crearPaneles();
        this.setResizable(false);
        this.setSize(1250, 720);
        this.setLayout(null);
        this.setLocationRelativeTo(this);
        this.setVisible(true);
    }

    private void crearPaneles() {

        pBarraSuperior = new JPanel();
        pBarraSuperior.setBounds(0, 0, 1250, 50);  
        pBarraSuperior.setLayout(null);
        this.add(pBarraSuperior);

        pCentral = new JPanel();
        pCentral.setBounds(0, 50, 1250, 360);
        pCentral.setLayout(null);
        this.add(pCentral);

        pRecorridos = new JPanel();
        pRecorridos.setBounds(0, 410, 1250, 275);
        pRecorridos.setLayout(null);
        pRecorridos.setVisible(false);
        this.add(pRecorridos);
    }

    public void aumentarFrame() {
        pRecorridos.setVisible(true);
    }

    public void disminuirFrame() {
        pRecorridos.setVisible(false);
    }

    public JPanel gPBarraSuperior() {
        return pBarraSuperior;
    }

    public JPanel gPCentral() {
        return pCentral;
    }

    public JPanel gPRecorridos() {
        return pRecorridos;
    }

	public void moverFrame(int i, int j) {

        this.setLocation(i, j);
	}
}
