package vista.componentes.barraSuperior;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/*
 * 
 * @author AndresFWilT
 * 
 */

public class BarraSuperiorTemplate extends JPanel {
    
    private static final long serialVersionUID = 1L;

    private JLabel lTitulo;
    private JButton bCerrar;
    private ImageIcon iCerrar;

    public BarraSuperiorTemplate(BarraSuperiorComponent barraSuperiorComponent) {

        lTitulo = new JLabel("NUMERO CROMATICO");
        lTitulo.setBounds( 305, 10, 650, 30);
        lTitulo.setHorizontalAlignment(SwingConstants.CENTER);
        lTitulo.setFont(new Font("Impact", Font.PLAIN, 20));
        lTitulo.setForeground(Color.WHITE);
        this.add(lTitulo);

        this.setSize(1250, 50);
        this.setLayout(null);
        this.setBackground(Color.black);
        this.addMouseListener(barraSuperiorComponent);
        this.addMouseMotionListener(barraSuperiorComponent);
    }

    public JButton gBCerrar() {
        return bCerrar;
    }
}
