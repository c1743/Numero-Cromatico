package vista.componentes.creacionGrafo;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import nodo.Vertice;
import vista.VistaPrincipalComponent;

/*
 * 
 * @author AndresFWilT
 * 
 */

public class PanelCreacionGrafoComponent implements MouseListener, ActionListener {

	private PanelCreacionGrafoTemplate panelCreacionGrafoTemplate;
	private VistaPrincipalComponent vistaPrincipalComponent;
	private int nVertices;
	private int[][] adyacencias;
	private boolean estadoFrame;

	public PanelCreacionGrafoComponent(VistaPrincipalComponent vistaPrincipalComponent) {
		this.vistaPrincipalComponent = vistaPrincipalComponent;
		nVertices = 1;
		estadoFrame = false;
		panelCreacionGrafoTemplate = new PanelCreacionGrafoTemplate(this);
	}

	public PanelCreacionGrafoTemplate gPanelCreacionGrafoTemplate() {
		return panelCreacionGrafoTemplate;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.getSource() instanceof JLabel) {
			Vertice vertice = new Vertice(e.getX(), e.getY(), nVertices);
			panelCreacionGrafoTemplate.nuevoVertice(vertice);
			nVertices++;
			panelCreacionGrafoTemplate.gLGrafo().repaint();
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if (e.getSource() instanceof JButton) {

			JButton boton = (JButton) e.getSource();
			boton.setBackground(new Color(20, 118, 131));
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if (e.getSource() instanceof JButton) {

			JButton boton = (JButton) e.getSource();
			boton.setBackground(new Color(0, 0, 0));
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String comando = e.getActionCommand();
		if (comando.equals("Adyacencia")) {

			panelCreacionGrafoTemplate.mostrarMatrizAdy();
			panelCreacionGrafoTemplate.gbDibujar().setEnabled(true);
		} else if (comando.equals("Numero Cromatico")) {
			estadoFrame = true;
			vistaPrincipalComponent.gVistaPrincipalTemplate().aumentarFrame();
			int t = panelCreacionGrafoTemplate.gNVertices(), i, j;
			String arista;
			adyacencias = new int[t][t];
			inicializarMatriz();
			for (i = 0; i < adyacencias.length; i++) {
				for (j = 0; j < adyacencias.length; j++) {
                    if (j > i) {
                        
                        arista = panelCreacionGrafoTemplate.gTArista(i, j).getText();
                        if (!arista.equals("")) {
                            adyacencias[i][j] = Integer.parseInt(arista);
                            adyacencias[j][i] = Integer.parseInt(arista);
                        }
                    }
				}
			}
			panelCreacionGrafoTemplate.dibujarArbol(adyacencias);
			vistaPrincipalComponent.recibeAdyacencias(adyacencias);
			panelCreacionGrafoTemplate.gbDibujar().setEnabled(true);
		} else {
			if (estadoFrame)
				vistaPrincipalComponent.gVistaPrincipalTemplate().disminuirFrame();

			this.nVertices = 1;
			panelCreacionGrafoTemplate.reiniciar();
			panelCreacionGrafoTemplate.gbListaAdy().setEnabled(true);
			panelCreacionGrafoTemplate.gbDibujar().setEnabled(true);
			estadoFrame = false;
		}
		panelCreacionGrafoTemplate.repaint();
	}

	public void inicializarMatriz() {
		for (int i = 0; i < adyacencias.length; i++) {
			for (int j = 0; j < adyacencias.length; j++) {
				adyacencias[i][j] = 0;
			}
		}
	}

	public int[][] gMatrizAdyacencias() {
		return adyacencias;
	}
}
