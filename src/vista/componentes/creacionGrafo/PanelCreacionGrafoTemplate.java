package vista.componentes.creacionGrafo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import nodo.Vertice;

/*
 * 
 * @author AndresFWilT
 * 
 */

public class PanelCreacionGrafoTemplate extends JPanel {

    private static final long serialVersionUID = 1L;
    private JPanel pAdyacencia;
    private JLabel lTitDibujar, lTitLista, lGrafo, lGrafoConLineas;
    private JButton bBorrar, bListaAdy, bDibujar;
    private ArrayList<Vertice> vertices;
    private JTextField[][] tAristas;
    private int[][] adyacencias;
    private JScrollPane scrollAdya;

    private Font fuente, fuente17;
    private Color colorFondoBotones, colorFondo;
    private Border borde;

    public PanelCreacionGrafoTemplate(PanelCreacionGrafoComponent panelCreacionGrafoComponent) {

        crearObjetosDecoradores();
        crearMenu(panelCreacionGrafoComponent);
        crearPanelesCreacion(panelCreacionGrafoComponent);

        this.setBounds(0, 0, 1250, 450);
        this.setLayout(null);
        this.setBackground(colorFondo);
    }

    public void reiniciar() {
        pAdyacencia.removeAll();
        vertices.clear();
        lGrafoConLineas.setVisible(false);
        lGrafo.setVisible(true);
    }

    public void dibujarArbol(int[][] adyacencias) {
        this.adyacencias = adyacencias;
        lGrafo.setVisible(false);
        lGrafoConLineas.setVisible(true);
    }

    public void mostrarMatrizAdy() {
        int t = vertices.size();
        tAristas = new JTextField[t][t];
        int x = 40, y = 30;
        for (int i = 0; i < t; i++) {
            JLabel lNumVertice2 = new JLabel(vertices.get(i).getNumero() + "");
            lNumVertice2.setBounds(x + 3, 5, 20, 20);
            lNumVertice2.setFont(fuente);
            pAdyacencia.add(lNumVertice2);
            x += 25;
        }
        for (int i = 0; i < t; i++) {
            x = 5;
            JLabel lNumVertice = new JLabel(vertices.get(i).getNumero() + "    ");
            lNumVertice.setBounds(x, y, 50, 20);
            lNumVertice.setFont(fuente);
            pAdyacencia.add(lNumVertice);
            x += 35;

            for (int j = 0; j < t; j++) {

                if (j > i) {
                    JTextField tArista = new JTextField();
                    tArista.setBounds(x, y, 20, 20);
                    tArista.setFont(fuente);
                    tArista.setBackground(Color.black);
                    tArista.setForeground(Color.white);
                    tAristas[i][j] = tArista;
                    pAdyacencia.add(tArista);
                }
                x += 25;
            }
            y += 25;
        }
        pAdyacencia.setPreferredSize(new Dimension(60 + t * 25, 50 + 25 * t));
        scrollAdya.doLayout();
    }

    public void nuevoVertice(Vertice vertice) {
        this.vertices.add(vertice);
    }

    private void crearPanelesCreacion(PanelCreacionGrafoComponent panelCreacionGrafoComponent) {
        vertices = new ArrayList<>();
        lGrafo = new JLabel() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            public void paint(Graphics g) {
                dibujarVertices(g);
            }
        };

        lGrafo.setBounds(5, 90, 620, 265);
        lGrafo.addMouseListener(panelCreacionGrafoComponent);
        this.add(lGrafo);

        lGrafoConLineas = new JLabel() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            public void paint(Graphics g) {
                dibujarVerticesConLineas(g);
            }
        };

        lGrafoConLineas.setBounds(5, 90, 645, 265);
        this.add(lGrafoConLineas);

        scrollAdya = new JScrollPane();
        scrollAdya.setBounds(625, 90, 605, 265);
        scrollAdya.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        scrollAdya.getViewport().setOpaque(true);
        scrollAdya.getViewport().setBackground(new Color(255, 255, 255));
        this.add(scrollAdya);

        pAdyacencia = new JPanel();
        pAdyacencia.setLayout(null);
        pAdyacencia.setOpaque(false);
        pAdyacencia.setPreferredSize(new Dimension(390, 290));
        scrollAdya.setViewportView(pAdyacencia);

    }

    protected void dibujarVerticesConLineas(Graphics g) {
        if (adyacencias == null)
            return;
        g.drawLine(0, 0, 615, 0);
        g.drawLine(615, 0, 615, 264);
        g.drawLine(615, 264, 0, 264);
        g.drawLine(0, 264, 0, 0);
        for (int i = 0; i < vertices.size(); i++) {

            for (int j = 0; j < adyacencias[i].length; j++) {
                if (adyacencias[i][j] != 0) {
                    g.drawLine(vertices.get(i).getX(), vertices.get(i).getY(), vertices.get(j).getX(),
                            vertices.get(j).getY());
                }
            }
        }

        g.setColor(Color.BLACK);
        g.setFont(fuente17);

        for (int i = 0; i < vertices.size(); i++) {
            if (vertices.get(i).getNumero() < 10) {
                g.setColor(colorFondo);
                g.fillRoundRect(vertices.get(i).getX() - 4, vertices.get(i).getY() - 17, 22, 20, 16, 16);
                g.setColor(Color.blue);
                g.drawRoundRect(vertices.get(i).getX() - 4, vertices.get(i).getY() - 17, 22, 20, 16, 16);
                g.drawString(vertices.get(i).getNumero() + "", vertices.get(i).getX(), vertices.get(i).getY());

            } else {

                g.setColor(colorFondo);
                g.fillRoundRect(vertices.get(i).getX() - 5, vertices.get(i).getY() - 17, 34, 20, 16, 16);
                g.setColor(Color.blue);
                g.drawRoundRect(vertices.get(i).getX() - 6, vertices.get(i).getY() - 18, 35, 21, 16, 16);
                g.drawString(vertices.get(i).getNumero() + "", vertices.get(i).getX(), vertices.get(i).getY());
            }
        }
    }

    protected void dibujarVertices(Graphics g) {
        g.setFont(fuente17);
        g.drawLine(0, 0, 615, 0);
        g.drawLine(615, 0, 615, 264);
        g.drawLine(615, 264, 0, 264);
        g.drawLine(0, 264, 0, 0);
        
        for (int i = 0; i < vertices.size(); i++) {
        	g.setColor(colorFondo);
            g.fillRoundRect(vertices.get(i).getX() - 5, vertices.get(i).getY() - 17, 20, 20, 16, 16);
            g.setColor(Color.blue);
            g.drawRoundRect(vertices.get(i).getX() - 6, vertices.get(i).getY() - 18, 20, 21, 16, 16);
            g.drawString(vertices.get(i).getNumero() + "", vertices.get(i).getX(), vertices.get(i).getY());
        }
    }

    private void crearObjetosDecoradores() {
        fuente = new Font("Impact", Font.PLAIN, 14);
        fuente17 = new Font("Impact", Font.PLAIN, 17);
        colorFondoBotones = new Color(0, 0, 0);
        colorFondo = new Color(255, 255, 255);
        borde = BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK);
    }

    private void crearMenu(PanelCreacionGrafoComponent panelCreacionGrafoComponent) {

        lTitDibujar = new JLabel("Grafo");
        lTitDibujar.setBounds(100, 5, 450, 35);
        lTitDibujar.setHorizontalAlignment(SwingConstants.CENTER);
        lTitDibujar.setFont(fuente17);
        this.add(lTitDibujar);

        lTitLista = new JLabel("Lista de adyacencia");
        lTitLista.setBounds(560, 5, 650, 35);
        lTitLista.setHorizontalAlignment(SwingConstants.CENTER);
        lTitLista.setFont(fuente17);
        this.add(lTitLista);

        bBorrar = new JButton("Limpiar panel");
        bBorrar.setBounds(20, 50, 140, 25);
        bBorrar.setFocusable(false);
        bBorrar.setFont(fuente);
        bBorrar.addActionListener(panelCreacionGrafoComponent);
        bBorrar.setBorder(null);
        bBorrar.setBackground(colorFondoBotones);
        bBorrar.setForeground(Color.WHITE);
        bBorrar.addMouseListener(panelCreacionGrafoComponent);
        this.add(bBorrar);

        bListaAdy = new JButton("Adyacencia");
        bListaAdy.setBounds(230, 50, 145, 25);
        bListaAdy.setFocusable(false);
        bListaAdy.setFont(fuente);
        bListaAdy.addActionListener(panelCreacionGrafoComponent);
        bListaAdy.setBorder(null);
        bListaAdy.setBackground(colorFondoBotones);
        bListaAdy.setForeground(Color.WHITE);
        bListaAdy.addMouseListener(panelCreacionGrafoComponent);
        this.add(bListaAdy);

        bDibujar = new JButton("Numero Cromatico");
        bDibujar.setBounds(435, 50, 165, 25);
        bDibujar.setFocusable(false);
        bDibujar.setFont(fuente);
        bDibujar.addActionListener(panelCreacionGrafoComponent);
        bDibujar.setBorder(null);
        bDibujar.setBackground(colorFondoBotones);
        bDibujar.setForeground(Color.WHITE);
        bDibujar.addMouseListener(panelCreacionGrafoComponent);
        bDibujar.setEnabled(false);
        this.add(bDibujar);
    }
    
    public JTextField[][] getTArista() {
    	return this.tAristas;
    }

    public JTextField gTArista(int i, int j) {
        return tAristas[i][j];
    }

    public JLabel gLGrafo() {
        return lGrafo;
    }

    public int gNVertices() {
        return vertices.size();
    }

    public JButton gbBorrarVertices() {
        return bBorrar;
    }

    public JButton gbDibujar() {
        return bDibujar;
    }

    public JButton gbListaAdy() {
        return bListaAdy;
    }

    public ArrayList<Vertice> getVertices() {
        return vertices;
    }

    public int[][] getAdyacencias() {
        return adyacencias;
    }

}
