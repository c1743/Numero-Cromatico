package vista.componentes.panelResultado;

import java.util.ArrayList;

import nodo.*;

/*
 * 
 * @author AndresFWilT
 * 
 */

public class PanelResultadoComponent {
    
    private PanelResultadoTemplate panelResultadoTemplate;

    public PanelResultadoComponent(ArrayList<Vertice> vertices) {

        panelResultadoTemplate = new PanelResultadoTemplate(vertices);
    }

    public PanelResultadoTemplate gPanelResultadoTemplate() {
        return panelResultadoTemplate;
    }

	public void dibujarResultados(int[][] adyacencias, ArrayList<ElementoColor> vColors, int nColores, String permutaciones) {
        panelResultadoTemplate.getlNumero().setText(nColores+"");
        panelResultadoTemplate.getlPermutaciones().setText(permutaciones);
        panelResultadoTemplate.dibujarGrafo(adyacencias, vColors, nColores);
	}
}
