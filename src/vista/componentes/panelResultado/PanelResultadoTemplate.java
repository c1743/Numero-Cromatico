package vista.componentes.panelResultado;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import nodo.ElementoColor;
import nodo.Vertice;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;

/*
 * 
 * @author AndresFWilT
 * 
 */

public class PanelResultadoTemplate extends JPanel {

    private static final long serialVersionUID = 1L;

    private JLabel lGrafoResultado, lResultados, lNCromatico, lNumero, lPermutacion, lPermutaciones;
    private ArrayList<Vertice> vertices;
    private ArrayList<ElementoColor> verticesColores;
    private ArrayList<Color> colores;
    private int[][] adyacencias;

    private Font fuente19, fuente;

    public PanelResultadoTemplate(ArrayList<Vertice> vertices) {

        fuente19 = new Font("Impact", Font.PLAIN, 17);
        fuente = new Font("Impact", Font.PLAIN, 14);

        this.vertices = vertices;
        colores = new ArrayList<>();
        crearColores();

        lResultados = new JLabel("Resultados");
        lResultados.setBounds(40, 30, 190, 35);
        lResultados.setHorizontalAlignment(SwingConstants.CENTER);
        lResultados.setFont(fuente19);
       // this.add(lResultados);

        lNCromatico = new JLabel("Numero Cromatico: ");
        lNCromatico.setBounds(930, 120, 250, 35);
        lNCromatico.setFont(new Font("Impact", Font.PLAIN, 24));
        this.add(lNCromatico);

        lNumero = new JLabel();
        lNumero.setBounds(1150, 120, 40, 40);
        lNumero.setFont(new Font("Impact", Font.PLAIN, 24));
        lNumero.setHorizontalAlignment(SwingConstants.CENTER);
        lNumero.setForeground(Color.blue);
        this.add(lNumero);
        
        lPermutacion = new JLabel("Permutacion:");
        lPermutacion.setBounds(20, 245, 150, 20);
        lPermutacion.setFont(new Font("Impact", Font.PLAIN, 24));
        this.add(lPermutacion);

        lPermutaciones = new JLabel();
        lPermutaciones.setBounds(120, 245, 450, 25);
        lPermutaciones.setFont(new Font("Impact", Font.PLAIN, 24));
        lPermutaciones.setForeground(Color.blue);
        lPermutaciones.setHorizontalAlignment(SwingConstants.CENTER);
        this.add(lPermutaciones);

        crearLabelGrafo();

        this.setSize(1250, 275);
        this.setLayout(null);
        this.setBackground(new Color(255, 255, 255));
    }

    private void crearLabelGrafo() {

        lGrafoResultado = new JLabel() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            public void paint(Graphics g) {
                dibujarGrafoResultado(g);
            }
        };

        lGrafoResultado.setBounds(350, 5, 490, 265);
        this.add(lGrafoResultado);
    }

    protected void dibujarGrafoResultado(Graphics g) {
        if (adyacencias == null || verticesColores == null)
            return;
        g.setFont(fuente19);
        for (int i = 0; i < vertices.size(); i++) {

            for (int j = 0; j < adyacencias[i].length; j++) {
                if (adyacencias[i][j] != 0) {
                    g.drawLine(vertices.get(i).getX(), vertices.get(i).getY(), vertices.get(j).getX(),
                            vertices.get(j).getY());
                }
                
            }
        }

        for (int i = 0; i < vertices.size(); i++) {

            for (int j = 0; j < adyacencias[i].length; j++) {
                if (vertices.get(i).getNumero()<10) {
                    
                    g.setColor(colores.get(verticesColores.get(i).getColor()));
                    g.fillRoundRect(vertices.get(i).getX() - 4, vertices.get(i).getY()-17, 22, 20, 16, 16);
                    g.setColor(Color.BLACK);
                    g.drawRoundRect(vertices.get(i).getX() - 4, vertices.get(i).getY()-17, 22, 20, 16, 16);
                    g.drawString(vertices.get(i).getNumero() + "", vertices.get(i).getX(), vertices.get(i).getY());
                } else {
                    g.setColor(colores.get(verticesColores.get(i).getColor()));
                    g.fillRoundRect(vertices.get(i).getX() - 5, vertices.get(i).getY()-17, 34, 20, 16, 16);
                    g.setColor(Color.BLACK);
                    g.drawRoundRect(vertices.get(i).getX() - 6, vertices.get(i).getY()-18, 35, 21, 16, 16);
                    g.drawString(vertices.get(i).getNumero() + "", vertices.get(i).getX(), vertices.get(i).getY());
                }
                
            }
        }        

    }

    public void dibujarGrafo(int[][] adyacencias, ArrayList<ElementoColor> vColors, int nColores) {
        this.adyacencias = adyacencias;
        this.verticesColores = vColors;
        
        lGrafoResultado.repaint();
    }

    private void crearColores() {
        Color c1 = new Color(0, 0, 255);
        colores.add(c1);
        Color c2 = new Color(255, 0, 0);
        colores.add(c2);
        Color c3 = new Color(0, 255, 0);
        colores.add(c3);
        Color c4 = new Color(255, 255, 0);
        colores.add(c4);
        Color c5 = new Color(0, 255, 255);
        colores.add(c5);
        Color c6 = new Color(255, 0, 255);
        colores.add(c6);
        Color c7 = new Color(150, 150, 150);
        colores.add(c7);
        Color c8 = new Color(255, 112, 0);
        colores.add(c8);
        Color c9 = new Color(0, 140, 28);
        colores.add(c9);
        Color c10 = new Color(95, 140, 0  );
        colores.add(c10);
        Color c11 = new Color(140, 83, 0 );
        colores.add(c11);
        Color c12 = new Color(255, 92, 85);
        colores.add(c12);
        Color c13 = new Color(255, 170, 85);
        colores.add(c13);
        Color c14 = new Color(55, 55, 55);
        colores.add(c14);
        Color c15 = new Color(113, 255, 85);
        colores.add(c15);
        Color c16 = new Color(85, 255, 201 );
        colores.add(c16);

    }

    public JLabel getlNumero() {
        return lNumero;
    }

    public JLabel getlPermutaciones() {
        return lPermutaciones;
    }

}
